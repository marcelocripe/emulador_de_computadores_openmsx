pt-BR:

O emulador de computadores openMSX foi criado pela equipe de desenvolvedores do openMSX, segundo a página eletrônica https://en.wikipedia.org/wiki/OpenMSX. Emula uma grande quantidade de sistemas baseados no MSX, entre eles, MSXturboR, Moonsound, IDE Controller da Sunrise, GFX9000, Pioneer Palcom LaserDisc, SpectraVideo SVI-318/328, ColecoVision e Sega SG-1000.

Contém: um arquivo de texto em idioma "pt-BR" explicando como proceder a instalação do emulador e o ícone de atalho (arquivo .desktop) traduzido.

Baixe ou descarregue ou transfira o arquivo de texto ".txt" e o arquivo compactado ".zip".

Todos os créditos e direitos estão incluídos nos arquivos, em respeito ao trabalho voluntário de cada pessoa que participou e colaborou para que estes arquivos pudessem ser disponibilizados nesta página eletrônica.

marcelocripe

- - - - -

de:

Der openMSX-Computeremulator wurde laut der Website https://en.wikipedia.org/wiki/OpenMSX vom openMSX-Entwicklerteam erstellt. Es emuliert eine große Anzahl von MSX-basierten Systemen, darunter MSXturboR, Moonsound, den IDE-Controller von Sunrise, GFX9000, Pioneer Palcom LaserDisc, SpectraVideo SVI-318/328, ColecoVision und Sega SG-1000.

Enthält: eine Textdatei in "pt-BR"-Sprache, die erklärt, wie der Emulator installiert wird, und das übersetzte Verknüpfungssymbol (.desktop-Datei).

Laden Sie die Textdatei „.txt“ und die komprimierte Datei „.zip“ herunter.

Alle Credits und Rechte sind in den Dateien enthalten, in Bezug auf die freiwillige Arbeit jeder Person, die teilgenommen und mitgearbeitet hat, damit diese Dateien auf dieser Website verfügbar gemacht werden konnten.

marcelocripe

- - - - -

en:

The openMSX computer emulator was created by the openMSX developer team, according to the website https://en.wikipedia.org/wiki/OpenMSX. It emulates a large number of MSX-based systems, including MSXturboR, Moonsound, Sunrise's IDE Controller, GFX9000, Pioneer Palcom LaserDisc, SpectraVideo SVI-318/328, ColecoVision and Sega SG-1000.

Contains: a text file in "pt-BR" language explaining how to install the emulator and the translated shortcut icon (.desktop file).

Download the ".txt" text file and the ".zip" compressed file.

All credits and rights are included in the files, in respect for the voluntary work of each person who participated and collaborated so that these files could be made available on this website.

marcelocripe

- - - - -

es:

El emulador de computadora openMSX fue creado por el equipo de desarrolladores de openMSX, según el sitio web https://en.wikipedia.org/wiki/OpenMSX. Emula una gran cantidad de sistemas basados ​​en MSX, incluidos MSXturboR, Moonsound, IDE Controller de Sunrise, GFX9000, Pioneer Palcom LaserDisc, SpectraVideo SVI-318/328, ColecoVision y Sega SG-1000.

Contiene: un archivo de texto en idioma "pt-BR" que explica cómo instalar el emulador y el icono de acceso directo traducido (archivo .desktop).

Descargue el archivo de texto ".txt" y el archivo comprimido ".zip".

Todos los créditos y derechos están incluidos en los archivos, en respeto al trabajo voluntario de cada persona que participó y colaboró ​​para que estos archivos pudieran estar disponibles en este sitio web.

marcelocripe

- - - - -

fr :

L'émulateur informatique openMSX a été créé par l'équipe de développeurs openMSX, selon le site Web https://en.wikipedia.org/wiki/OpenMSX. Il émule un grand nombre de systèmes basés sur MSX, notamment MSXturboR, Moonsound, Sunrise's IDE Controller, GFX9000, Pioneer Palcom LaserDisc, SpectraVideo SVI-318/328, ColecoVision et Sega SG-1000.

Contient : un fichier texte en langage "pt-BR" expliquant comment installer l'émulateur et l'icône de raccourci traduite (fichier .desktop).

Téléchargez le fichier texte ".txt" et le fichier compressé ".zip".

Tous les crédits et droits sont inclus dans les fichiers, dans le respect du travail bénévole de chaque personne qui a participé et collaboré afin que ces fichiers puissent être mis à disposition sur ce site.

marcelocripe

- - - - -

it:

L'emulatore di computer openMSX è stato creato dal team di sviluppatori di openMSX, secondo il sito Web https://en.wikipedia.org/wiki/OpenMSX. Emula un gran numero di sistemi basati su MSX, inclusi MSXturboR, Moonsound, controller IDE di Sunrise, GFX9000, Pioneer Palcom LaserDisc, SpectraVideo SVI-318/328, ColecoVision e Sega SG-1000.

Contiene: un file di testo in lingua "pt-BR" che spiega come installare l'emulatore e l'icona del collegamento tradotto (file .desktop).

Scarica il file di testo ".txt" e il file compresso ".zip".

Tutti i crediti ei diritti sono inclusi nei file, nel rispetto del lavoro volontario di ogni persona che ha partecipato e collaborato affinché questi file potessero essere resi disponibili su questo sito web.

marcelocripe
